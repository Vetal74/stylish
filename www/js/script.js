/**
 * Created by root on 12.09.16.
 */

$(function() {



    $(".block-1 .line-slick .tabG").slick({
        infinite: true,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: '<div class="sprite sprite-arrowNext"></div>',
        prevArrow: '<div class="sprite sprite-arrowPrev"></div>'
    });
    $(".block-1 .line-slick .tabM").slick({
        infinite: true,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: '<div class="sprite sprite-arrowNext"></div>',
        prevArrow: '<div class="sprite sprite-arrowPrev"></div>'
    });
    $(".block-2 .line-slick .tabG").slick({
        infinite: true,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: '<div class="sprite sprite-arrowNext"></div>',
        prevArrow: '<div class="sprite sprite-arrowPrev"></div>'
    });
    $(".block-2 .line-slick .tabM").slick({
        infinite: true,
        autoplay: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        nextArrow: '<div class="sprite sprite-arrowNext"></div>',
        prevArrow: '<div class="sprite sprite-arrowPrev"></div>'
    });

    var tumbler = true;
    $(".block-1 .tabs > div").click(function() {
        if($(this).hasClass("active")) {
            return false;
        } else {
            $(".block-1 .tabs > div").removeClass("active");
            $(this).addClass("active");
        }
        $(".block-1 .line-slick").addClass("active");
        var op = $(".block-1 .line-slick .slider.active");
        setTimeout(function() {
            if(tumbler) {
                op.toggleClass("hidden active");
                op.next().toggleClass("hidden active");
                op.next().slick('slickNext');
                tumbler = false;
            } else {
                op.toggleClass("hidden active");
                op.prev().toggleClass("hidden active");
                op.prev().slick('slickNext');
                tumbler = true;
            }

            $(".block-1 .line-slick").removeClass("active");
        }, 500);
        return false;
    });

    var tumbler1 = true;
    $(".block-2 .tabs > div").click(function() {
        if($(this).hasClass("active")) {
            return false;
        } else {
            $(".block-2 .tabs > div").removeClass("active");
            $(this).addClass("active");
        }
        $(".block-2 .line-slick").addClass("active");
        var op = $(".block-2 .line-slick .slider.active");
        setTimeout(function() {
            if(tumbler1) {
                op.toggleClass("hidden active");
                op.next().toggleClass("hidden active");
                op.next().slick('slickNext');
                tumbler1 = false;
            } else {
                op.toggleClass("hidden active");
                op.prev().toggleClass("hidden active");
                op.prev().slick('slickNext');
                tumbler1 = true;
            }

            $(".block-2 .line-slick").removeClass("active");
        }, 500);
        return false;
    });

    $(".navigate .search .sprite").click(function() {
        $(".navigate .search .hidden").addClass("active");
    });

    $(".cart").click(function() {
        $(".cart .sprite-tb").toggleClass("active");
        $(".cart-go").toggleClass("active");
    });

    $(".catalog .item").hover(function () {
        $(this).toggleClass("active");
    }, function () {
        $(this).toggleClass("active");
    });

    $("aside .filters .line").click(function() {
        $(this).toggleClass("active");
    });

});
